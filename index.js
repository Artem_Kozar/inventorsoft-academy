"use strict";

const merge = (arr1, arr2) => {
    let sorted = [];

    while (arr1.length && arr2.length) {
        console.log('arr1 -',arr1[0], "-" ,'arr2 -', arr2[0]);
        console.log(`sorted - ${sorted}`);
        if (arr1[0] < arr2[0]) sorted.push(arr1.shift());
        else sorted.push(arr2.shift());
        // console.log(arr1[0], "-" ,arr2[0]);
        // console.log(`sorted - ${sorted}`);
    }

    return sorted.concat(arr1.slice().concat(arr2.slice()));
};

const mergeSort = arr => {
    if (arr.length <= 1) return arr;
    let mid = Math.floor(arr.length / 2),
        left = mergeSort(arr.slice(0, mid)),
        right = mergeSort(arr.slice(mid));

     // console.log('arr -',arr);
     // console.log(`Left - ${left}, Right - ${right}`);
    return merge(left, right);
};

function isNum(value) {
    return !isNaN(value);


}

let valueOfTextArea2 = document.querySelector('#Textarea2');
let valueOfTextArea1 = document.querySelector('#Textarea1');

document.querySelector("#btn").onclick = () => {

    let arr = valueOfTextArea1.value.split(',');
    let arr1 = arr.map(Number);

    if (arr.every(isNum)) {
        document.querySelector('#message').style.visibility = 'hidden';
        document.querySelector('#arrLenght').innerHTML = arr.length;
        valueOfTextArea2.value = mergeSort(arr1);

    } else {
        document.querySelector('#message').style.visibility = 'visible';
        document.querySelector('#arrLenght').innerHTML = '';
        valueOfTextArea2.value = '';
    }

};



